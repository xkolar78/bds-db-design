CREATE SCHEMA IF NOT EXISTS `bds_db_design` DEFAULT CHARACTER SET utf8 ;
USE `bds_db_design` ;


CREATE TABLE IF NOT EXISTS bds_db_design.address
(
    id_address integer NOT NULL,
    city text NOT NULL,
    house_number text NOT NULL,
    street text NOT NULL,
    zip_code text NOT NULL,
    PRIMARY KEY (id_address)
);

CREATE TABLE IF NOT EXISTS bds_db_design.building
(
    id_building integer NOT NULL,
    building_type text NOT NULL,
    building_description text NOT NULL,
    PRIMARY KEY (id_building)
);

CREATE TABLE IF NOT EXISTS bds_db_design.dosing_list
(
    id_dosing_list integer NOT NULL,
    id_patient integer NOT NULL,
    PRIMARY KEY (id_dosing_list)
);

CREATE TABLE IF NOT EXISTS bds_db_design.dosing_to_list
(
    dosing_value text NOT NULL,
    dose_description text,
    dose_time text NOT NULL,
    id_medication integer,
    id_dosing_list integer NOT NULL
);

CREATE TABLE IF NOT EXISTS bds_db_design.employee
(
    id_employee integer NOT NULL,
    given_name text NOT NULL,
    family_name text NOT NULL,
    mail text NOT NULL,
    phone_number integer NOT NULL,
    photo text NOT NULL,
    PRIMARY KEY (id_employee)
);

CREATE TABLE IF NOT EXISTS bds_db_design.employee_has_address
(
    id_address integer,
    id_employee integer
);

CREATE TABLE IF NOT EXISTS bds_db_design.medication
(
    id_medication integer NOT NULL,
    medication_type text NOT NULL,
    medication_description text NOT NULL,
    recommended_dose_value text NOT NULL,
    PRIMARY KEY (id_medication)
);

CREATE TABLE IF NOT EXISTS bds_db_design.patient
(
    id_patient integer NOT NULL,
    given_name text NOT NULL,
    family_name text NOT NULL,
    phone_number integer NOT NULL,
    family_contact integer NOT NULL,
    photo text,
    PRIMARY KEY (id_patient)
);

CREATE TABLE IF NOT EXISTS bds_db_design.patient_has_address
(
    id_patient integer,
    id_address integer
);

CREATE TABLE IF NOT EXISTS bds_db_design.position
(
    id_employee integer,
    position_value text NOT NULL
);

CREATE TABLE IF NOT EXISTS bds_db_design.procedure
(
    id_procedure integer NOT NULL,
    id_room integer NOT NULL,
    date date NOT NULL,
    time_start text NOT NULL,
    procedure_type text NOT NULL,
    topic text NOT NULL,
    id_patient integer,
    id_employee integer,
    PRIMARY KEY (id_procedure)
);

CREATE TABLE IF NOT EXISTS bds_db_design.room
(
    id_room integer NOT NULL,
    id_building integer,
    floor integer NOT NULL,
    room_number integer NOT NULL,
    PRIMARY KEY (id_room)
);

CREATE TABLE IF NOT EXISTS bds_db_design.stay
(
    id_stay integer NOT NULL,
    id_patient integer,
    id_room integer,
    start_date text NOT NULL,
    end_date text NOT NULL,
    PRIMARY KEY (id_stay)
);

ALTER TABLE bds_db_design.dosing_list
    ADD FOREIGN KEY (id_patient)
    REFERENCES bds_db_design.patient (id_patient);


ALTER TABLE bds_db_design.dosing_to_list
    ADD FOREIGN KEY (id_medication)
    REFERENCES bds_db_design.medication (id_medication);


ALTER TABLE bds_db_design.dosing_to_list
    ADD FOREIGN KEY (id_dosing_list)
    REFERENCES bds_db_design.dosing_list (id_dosing_list);


ALTER TABLE bds_db_design.employee_has_address
    ADD FOREIGN KEY (id_address)
    REFERENCES bds_db_design.address (id_address);


ALTER TABLE bds_db_design.employee_has_address
    ADD FOREIGN KEY (id_employee)
    REFERENCES bds_db_design.employee (id_employee);


ALTER TABLE bds_db_design.patient_has_address
    ADD FOREIGN KEY (id_address)
    REFERENCES bds_db_design.address (id_address);


ALTER TABLE bds_db_design.patient_has_address
    ADD FOREIGN KEY (id_patient)
    REFERENCES bds_db_design.patient (id_patient);


ALTER TABLE bds_db_design.position
    ADD FOREIGN KEY (id_employee)
    REFERENCES bds_db_design.employee (id_employee);


ALTER TABLE bds_db_design.procedure
    ADD FOREIGN KEY (id_employee)
    REFERENCES bds_db_design.employee (id_employee);


ALTER TABLE bds_db_design.procedure
    ADD FOREIGN KEY (id_patient)
    REFERENCES bds_db_design.patient (id_patient);


ALTER TABLE bds_db_design.procedure
    ADD FOREIGN KEY (id_room)
    REFERENCES bds_db_design.room (id_room);


ALTER TABLE bds_db_design.room
    ADD FOREIGN KEY (id_building)
    REFERENCES bds_db_design.building (id_building);


ALTER TABLE bds_db_design.stay
    ADD FOREIGN KEY (id_patient)
    REFERENCES bds_db_design.patient (id_patient);


ALTER TABLE bds_db_design.stay
    ADD FOREIGN KEY (id_room)
    REFERENCES bds_db_design.room (id_room);



INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (1, 'Osek', '72', 'Františka Černého', '229 87');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (2, 'Rýmařov', '478', 'Před Rybníkem', '596 85');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (3, 'Pečky', '5', 'Juarézova', '462 52');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (4, 'Rýmařov', '82', 'Poleradská', '761 81');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (5, 'Chomutov', '2', 'Tanvaldská', '136 55');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (6, 'Kardašova Řečice', '96', 'U Waltrovky', '427 93');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (7, 'Chvaletice', '7', 'Rokytnická', '623 17');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (8, 'Kdyně', '16', 'Spinozova', '587 27');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (9, 'Jablonec nad Jizerou', '40', 'K Hořavce', '539 48');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (10, 'Rousínov', '5', 'U Hostivařského Nádraží', '545 04');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (11, 'Horní Blatná', '442', 'Musílkova', '496 63');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (12, 'Pacov', '6', 'Vratislavova', '473 16');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (13, 'Hoštka', '73', 'Tachovské Náměstí', '221 69');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (14, 'Přerov', '130', 'Anny Drabíkové', '116 82');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (15, 'Jablonec nad Jizerou', '318', 'Dřítenská', '720 30');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (16, 'Jablonec nad Jizerou', '11', 'Na Příčné Mezi', '348 25');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (17, 'Náměšť nad Oslavou', '759', 'Meziškolská', '116 71');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (18, 'Kralovice', '578', 'Na Šafránce', '709 53');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (19, 'Kravaře', '12', 'K Vidouli', '173 29');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (20, 'Olomouc', '55', 'Sitteho', '399 11');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (21, 'Olomouc', '1', 'Anny Drabíkové', '428 00');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (22, 'Mašťov', '460', 'K Dálnici', '732 43');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (23, 'Mýto', '6', 'Za Stadionem', '484 37');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (24, 'Kamenice nad Lipou', '275', 'Čankovská', '463 98');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (25, 'Žlutice', '431', 'V Nížinách', '235 06');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (26, 'Kouřim', '54', 'Severní Viii', '723 49');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (27, 'Lanžhot', '207', 'Maňákova', '653 72');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (28, 'Mirovice', '6', 'Slatiňanská', '189 71');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (29, 'Planá nad Lužnicí', '385', 'Paťanka', '200 71');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (30, 'Valtice', '44', 'Pohořelec', '113 40');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (31, 'Žebrák', '909', 'U Nové Dálnice', '713 02');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (32, 'Vimperk', '85', 'Josefa Šimůnka', '634 14');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (33, 'Sázava', '64', 'Plynární', '357 09');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (34, 'Luhačovice', '991', 'K Přehradám', '205 44');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (35, 'Dašice', '377', 'Ostrovského', '170 12');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (36, 'Rtyně v Podkrkonoší', '7', 'Jihovýchodní I', '332 61');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (37, 'Přimda', '398', 'Budapešťská', '590 93');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (38, 'Liberec', '183', 'Šachovská', '744 25');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (39, 'Horažďovice', '78', 'Na Šubě', '314 84');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (40, 'Třemošnice', '704', 'K Vidouli', '329 44');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (41, 'Libčice nad Vltavou', '57', 'Do Zahrádek Ii', '186 30');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (42, 'Vlachovo Březí', '1', 'Horní Chaloupky', '475 89');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (43, 'Mimoň', '379', 'Dobrošovská', '628 49');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (44, 'Valašské Klobouky', '24', 'Heydukova', '291 90');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (45, 'Žandov', '6', 'Jilmová', '650 89');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (46, 'Benešov nad Ploučnicí', '14', 'V Padolině', '753 58');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (47, 'Zákupy', '322', 'K Betáni', '206 53');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (48, 'Moravský Beroun', '8', 'Ke Břvům', '783 54');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (49, 'Rudolfov', '1', 'Za Valem', '417 80');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (50, 'Česká Skalice', '18', 'Nad Krocínkou', '749 90');
INSERT INTO bds_db_design.address (id_address, city, house_number, street, zip_code) VALUES (51, 'Týnec nad Sázavou', '339', 'Augustova', '528 11');

INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (1, 'paralen', 'proti bolesti', '2 tablety');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (2, 'ibalgin', 'tlumeni bolesti', '1 tableta');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (3, 'xanax', 'antidepresivum', 'polovina tablety');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (4, 'zenaro', 'proti alergii', '1x denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (5, 'actos', 'náhrada za metaformin(cukrovka)', '15-30mg denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (6, 'Actrapid', 'inzulin', 'injekčně 1x ampulka');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (7, 'Artrodar', 'proti zánětům na klouby', '2 toboly 50mg denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (8, 'Aryzalera', 'maniodeprese a schizofrenie', '15-30mg 1x denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (9, 'Aspulmo', 'uvolnění průdušek', 'inhalace 1-2 vdechů nárazově při záchvatu');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (10, 'Atram', 'snížení tlaku a zklidnění', '2x denně od 6,25 do 25mg');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (11, 'Axetine', 'streptokokové infekce dýchacích cest', '3x denně roztokxinfuze 750x1500mg');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (12, 'Bisocard', 'snižuje krevní tlak a stres, proti srdeční poruše rytmu', '1x denně 2,5-10mg');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (13, 'Brieka', 'proti epileptickým záchvatům', '150-600mg ve 3 dávkách denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (14, 'Budenofalk', 'střevní záněty', '9mg v podání rektální pěna pommocí aplikátoru');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (15, 'Bupainx', 'opioidní anelgetikum', '1 náplast interval výměny 3-4 dny');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (16, 'Caduet', 'snížení cholesterolu', '5-10mg denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (17, 'Calgel', 'lokální anestetikum na dásně a afty', 'několikrát denně vmasírovat do dutiny ústní');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (18, 'Canocord', 'snížení tlaku po infarktecha srdečních příhodách', '1x denně 4-32mg nepřevyšovat maximální dávku!');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (19, 'Carbomedac', 'zhoubné nádory vaječníků a plic', 'pomalá nitrožilní infuze - rozhoduje onkolog');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (20, 'Coryol', 'snížení tlaku a bušení srdce', '2 tobolky denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (21, 'Dacogen', 'akutní leukemie', 'nitrožilně, roztok dle onkologa');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (22, 'Daylette', 'zabránění nechtěnému otěhotnění', '3 týdny 1 tableta denně 1 týden pauza');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (23, 'Desloratadine', 'alergická rýma a kopřivka', '5mg denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (24, 'Diecyclen', 'zabránění nechtěnému otěhotnění', '3 týdny 1 tableta denně 1 týden pauza');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (25, 'Dobutamin', 'zvýšení krevního tlaku', '2-10 mikrogramů na 1kg tělesé hmoty');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (26, 'Elidel', 'chronické záněty kůže', 'krém 2x denně na poostižená místa');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (27, 'Egolanza', 'léčba schizofrenie a maniodeprese', '5-20mg denně dle pacienta');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (28, 'Encephabol', 'prokrvení mozku a zlepšení paměti', '300-600mg denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (29, 'Erclany', 'proti epileptickým záchvatům', '150-600mg ve 3 dávkách denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (30, 'Eucarbon', 'léčba zácpy', '1-2 tablety 2-3x denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (31, 'Fenofix', 'snížení hladiny tuků', '1 tableta denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (32, 'Fluoxetin', 'poruchy nálady', '1 tableta 1x denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (33, 'Fyssagir', 'zabránění nechtěnému otěhotnění', '3 týdny 1 tableta denně 1 týden pauza');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (34, 'Gendron', 'osteopróza a řídnutí kostí', '1 tableta 1x denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (35, 'Gliclazide', 'proti cukrovce', '30mg denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (36, 'Gutron', 'zvýšení krevního tlaku', '2,5mg 2x denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (37, 'Hartil-H', 'zmenšení krevního tlaku', '1 tabletka 1x denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (38, 'Helicid', 'sklidnění žaludku', '20-40mg denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (39, 'Hyalgan', 'boest kloubů', 'injekčně do kloubu 1x denně po dobu 5 týdnů');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (40, 'Hypnomidate', 'celková anestézie', 'rozhoduje lekař');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (41, 'Ibandronát', 'proti řídnutí kostí', '150mg denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (42, 'Ibument', 'proti bolesti', '2 tebletky 2x denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (43, 'Ichtoxyl', 'proti ekzémům', 'potírat 3x denně postižená místa');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (44, 'Ilibrift', 'pro zpomalení srdce', '5mg 2x denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (45, 'Imunor', 'poruchy imunity', '4-6 lahviček v týdenních intervalech');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (46, 'Iprofenex', 'proti bolesti', '2 tablety 1x denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (47, 'Litak', 'léčba leukémie', 'injekčně dle lékaře');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (48, 'Megace', 'zvýšení chuti k jídlu', '600-800mg denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (49, 'Migraptan', 'proti stresu', '50mg denně');
INSERT INTO bds_db_design.medication (id_medication, medication_type, medication_description, recommended_dose_value) VALUES (50, 'Neupogen', 'zvýšení bílých krvinek', '1x injekčně denně');

INSERT INTO bds_db_design.building (id_building, building_type, building_description) VALUES (1, 'dětská nemocnice', 'růžová budova');
INSERT INTO bds_db_design.building (id_building, building_type, building_description) VALUES (2, 'porodnice', 'zelená budova');
INSERT INTO bds_db_design.building (id_building, building_type, building_description) VALUES (3, 'chirurgie', 'modrá budova');
INSERT INTO bds_db_design.building (id_building, building_type, building_description) VALUES (4, 'plastická chirurgie', 'žlutá budova');
INSERT INTO bds_db_design.building (id_building, building_type, building_description) VALUES (5, 'jednotka intenzivní péče', 'červená budova');

INSERT INTO bds_db_design.room (id_room, id_building, floor, room_number) VALUES (1, 1, 1, 101);
INSERT INTO bds_db_design.room (id_room, id_building, floor, room_number) VALUES (2, 1, 2, 102);
INSERT INTO bds_db_design.room (id_room, id_building, floor, room_number) VALUES (3, 2, 1, 201);
INSERT INTO bds_db_design.room (id_room, id_building, floor, room_number) VALUES (4, 2, 2, 202);
INSERT INTO bds_db_design.room (id_room, id_building, floor, room_number) VALUES (5, 3, 1, 301);
INSERT INTO bds_db_design.room (id_room, id_building, floor, room_number) VALUES (6, 3, 2, 302);
INSERT INTO bds_db_design.room (id_room, id_building, floor, room_number) VALUES (7, 4, 1, 401);
INSERT INTO bds_db_design.room (id_room, id_building, floor, room_number) VALUES (8, 4, 2, 402);
INSERT INTO bds_db_design.room (id_room, id_building, floor, room_number) VALUES (9, 5, 1, 501);
INSERT INTO bds_db_design.room (id_room, id_building, floor, room_number) VALUES (10, 5, 2, 502);


INSERT INTO bds_db_design.patient (id_patient, given_name, family_name, phone_number, family_contact, photo) VALUES (1, 'Adam', 'Velký', 456954875, 146789546, 'photo.url');
INSERT INTO bds_db_design.patient (id_patient, given_name, family_name, phone_number, family_contact, photo) VALUES (2, 'Lukáš', 'Petrášek', 741852963, 756654456, 'photo.url');
INSERT INTO bds_db_design.patient (id_patient, given_name, family_name, phone_number, family_contact, photo) VALUES (3, 'Daniel', 'Legner', 123456789, 852369456, 'photo.url');
INSERT INTO bds_db_design.patient (id_patient, given_name, family_name, phone_number, family_contact, photo) VALUES (4, 'Ondřej', 'Hradil', 951357852, 212345682, 'photo.url');
INSERT INTO bds_db_design.patient (id_patient, given_name, family_name, phone_number, family_contact, photo) VALUES (5, 'Miloslav', 'Kroneisl', 777536945, 123456789, 'photo.url');


INSERT INTO bds_db_design.dosing_list (id_dosing_list, id_patient) VALUES (1, 2);
INSERT INTO bds_db_design.dosing_list (id_dosing_list, id_patient) VALUES (2, 1);
INSERT INTO bds_db_design.dosing_list (id_dosing_list, id_patient) VALUES (3, 4);
INSERT INTO bds_db_design.dosing_list (id_dosing_list, id_patient) VALUES (4, 3);
INSERT INTO bds_db_design.dosing_list (id_dosing_list, id_patient) VALUES (5, 5);


INSERT INTO bds_db_design.dosing_to_list (dosing_value, dose_description, dose_time, id_medication, id_dosing_list) VALUES ('5mg', '5mg rozděleno do dvou dávek nitrožilně 2,5mg ráno a 2,5mg večer', '10:00, 18:00', 36, 1);
INSERT INTO bds_db_design.dosing_to_list (dosing_value, dose_description, dose_time, id_medication, id_dosing_list) VALUES ('20ml', 'roztírat na postižená místa 3x denně', '08:00, 12:00, 18:00', 43, 2);
INSERT INTO bds_db_design.dosing_to_list (dosing_value, dose_description, dose_time, id_medication, id_dosing_list) VALUES ('50ml', 'vnitrožilně 2x 25ml denně', '11:00, 23:00', 47, 3);
INSERT INTO bds_db_design.dosing_to_list (dosing_value, dose_description, dose_time, id_medication, id_dosing_list) VALUES ('650mg', '3x denně (200mg, 200mg, 250mg)', '04:00, 12:00, 21:00', 13, 4);
INSERT INTO bds_db_design.dosing_to_list (dosing_value, dose_description, dose_time, id_medication, id_dosing_list) VALUES ('1 tableta', '1x tableta denně po dobu 3 týdnů, týden pauza', '18:00', 22, 5);


INSERT INTO bds_db_design.employee (id_employee, given_name, family_name, mail, phone_number, photo) VALUES (2, 'Marek', 'Vávra', 'marekvavra@seznam.cz', 456584215, 'photo.url');
INSERT INTO bds_db_design.employee (id_employee, given_name, family_name, mail, phone_number, photo) VALUES (3, 'Ondřej', 'Kodl', 'ondrejkodl@seznam.cz', 741258963, 'photo.url');
INSERT INTO bds_db_design.employee (id_employee, given_name, family_name, mail, phone_number, photo) VALUES (4, 'Hugo', 'Toxxx', 'hugotoxxx@seznam.cz', 753951258, 'photo.url');
INSERT INTO bds_db_design.employee (id_employee, given_name, family_name, mail, phone_number, photo) VALUES (5, 'Radim', 'Ošklivý', 'radimosklivy@seznam.cz', 789456123, 'photo.url');
INSERT INTO bds_db_design.employee (id_employee, given_name, family_name, mail, phone_number, photo) VALUES (1, 'Jan', 'Novák', 'jannovak@seznam.cz', 452125478, 'photo.url');


INSERT INTO bds_db_design.employee_has_address (id_address, id_employee) VALUES (8, 1);
INSERT INTO bds_db_design.employee_has_address (id_address, id_employee) VALUES (27, 2);
INSERT INTO bds_db_design.employee_has_address (id_address, id_employee) VALUES (35, 3);
INSERT INTO bds_db_design.employee_has_address (id_address, id_employee) VALUES (29, 4);
INSERT INTO bds_db_design.employee_has_address (id_address, id_employee) VALUES (50, 5);

INSERT INTO bds_db_design.patient_has_address (id_patient, id_address) VALUES (1, 5);
INSERT INTO bds_db_design.patient_has_address (id_patient, id_address) VALUES (2, 7);
INSERT INTO bds_db_design.patient_has_address (id_patient, id_address) VALUES (3, 15);
INSERT INTO bds_db_design.patient_has_address (id_patient, id_address) VALUES (4, 37);
INSERT INTO bds_db_design.patient_has_address (id_patient, id_address) VALUES (5, 45);


INSERT INTO bds_db_design.position (id_employee, position_value) VALUES (1, 'primář');
INSERT INTO bds_db_design.position (id_employee, position_value) VALUES (2, 'chirurg');
INSERT INTO bds_db_design.position (id_employee, position_value) VALUES (3, 'pomocná sestra');
INSERT INTO bds_db_design.position (id_employee, position_value) VALUES (4, 'řidič sanitky');
INSERT INTO bds_db_design.position (id_employee, position_value) VALUES (5, 'správce');


INSERT INTO bds_db_design.procedure (id_procedure, id_room, date, time_start, procedure_type, topic, id_patient, id_employee) VALUES (1, 6, '2021-10-31', '12:00', 'transplantace srdce', '', 1, 1);
INSERT INTO bds_db_design.procedure (id_procedure, id_room, date, time_start, procedure_type, topic, id_patient, id_employee) VALUES (2, 5, '2021-11-11', '08:00', 'plastické zvětšení prsou', 'velikost H', 5, 2);
INSERT INTO bds_db_design.procedure (id_procedure, id_room, date, time_start, procedure_type, topic, id_patient, id_employee) VALUES (3, 5, '2021-11-13', '14:00', 'zmenšení nosu', '', 2, 1);
INSERT INTO bds_db_design.procedure (id_procedure, id_room, date, time_start, procedure_type, topic, id_patient, id_employee) VALUES (4, 6, '2021-12-25', '15:00', 'katetrizace srdce', '', 4, 2);
INSERT INTO bds_db_design.procedure (id_procedure, id_room, date, time_start, procedure_type, topic, id_patient, id_employee) VALUES (5, 3, '2022-01-01', 'dle situace', 'porod', 'dvojčata, císařský řez', 3, 2);


INSERT INTO bds_db_design.stay (id_stay, id_patient, id_room, start_date, end_date) VALUES (1, 5, 2, '28.10.2021', '05.11.2021');
INSERT INTO bds_db_design.stay (id_stay, id_patient, id_room, start_date, end_date) VALUES (2, 4, 1, '27.10.2021', '06.11.2021');
INSERT INTO bds_db_design.stay (id_stay, id_patient, id_room, start_date, end_date) VALUES (3, 3, 4, '18.09.2021', '05.10.2021');
INSERT INTO bds_db_design.stay (id_stay, id_patient, id_room, start_date, end_date) VALUES (4, 1, 3, '15.09.2021', '15.12.2021');
INSERT INTO bds_db_design.stay (id_stay, id_patient, id_room, start_date, end_date) VALUES (5, 2, 5, '01.06.2021', '01.01.2022');

